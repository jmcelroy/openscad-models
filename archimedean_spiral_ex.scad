// spirals     = how many spirals (positive=CCW, negative=CW)
// thickness   = how thick you want the arms to be
// startradius = beginning radius position
// spacing     = spacing between radial points
// startangle  = angle to begin first sweep
// center      = 'true' centers the spiral arm thickness

$fn = 100;
$fa=1;

module archimedean_spiral_ex(spirals=10, thickness=0.1, spacing = 0.5, startradius = 1.0, startangle = 0, center=true) {
	t = thickness;
	s1 = (startradius / spacing) * 360.0;
	s2 = ((startradius + (abs(spirals)*spacing)) / spacing) * 360.0;
	angoff = ((-s1+startangle) % 360.0);
	a = sqrt(pow(spacing*abs(spirals)+startradius,2)/(pow(s2,2)*(pow(cos(s2),2) + pow(sin(s2),2))));
	points=[
		for(i = [s1+($fa - (s1 % $fa))-$fa:$fa:s2+($fa - (s2 % $fa))-$fa]) [
			(((i*a)+ ((center) ? -t/2 : 0))*cos(sign(spirals)*(i+angoff))),
			(((i*a)+ ((center) ? -t/2 : 0))*sin(sign(spirals)*(i+angoff)))
		]
	];
	points_inner=[
		for(i = [s2+($fa - (s2 % $fa))-$fa:-$fa:s1+($fa - (s1 % $fa))-$fa]) [
			(((i*a)+ ((center) ? t/2 : t))*cos(sign(spirals)*(i+angoff))),
			(((i*a)+ ((center) ? t/2 : t))*sin(sign(spirals)*(i+angoff)))
		]
	];
	polygon(concat(points,points_inner));
}

module archimedean_spiral_ex_flip(spirals=10, thickness=0.1, spacing = 0.5, startradius = 1.0, startangle = 0, center=true) {
	t = thickness;
	s1 = (startradius / spacing) * 360.0;
	s2 = ((startradius + (abs(spirals)*spacing)) / spacing) * 360.0;
	angoff = ((-s1+startangle) % 360.0);
	a = sqrt(pow(spacing*abs(spirals)+startradius,2)/(pow(s2,2)*(pow(cos(s2),2) + pow(sin(s2),2))));
	points=[
		for(i = [s1+($fa - (s1 % $fa))-$fa:$fa:s2+($fa - (s2 % $fa))-$fa]) [
			-(((i*a)+ ((center) ? -t/2 : 0))*cos(sign(spirals)*(i+angoff))),
			-(((i*a)+ ((center) ? -t/2 : 0))*sin(sign(spirals)*(i+angoff)))
		]
	];
	points_inner=[
		for(i = [s2+($fa - (s2 % $fa))-$fa:-$fa:s1+($fa - (s1 % $fa))-$fa]) [
			-(((i*a)+ ((center) ? t/2 : t))*cos(sign(spirals)*(i+angoff))),
			-(((i*a)+ ((center) ? t/2 : t))*sin(sign(spirals)*(i+angoff)))
		]
	];
	polygon(concat(points,points_inner));
}



linear_extrude(height=0.2) {

    archimedean_spiral_ex(spirals=7, thickness=0.1, spacing = 0.5, startradius = 1, startangle = 0, center=true);
    
    archimedean_spiral_ex_flip(spirals=7, thickness=0.1, spacing = 0.5, startradius = 1, startangle = 0, center=true);
    
    //archimedean_spiral_ex_flip(spirals=0.5, thickness=0.1, spacing = 0.5, startradius = 0, startangle = 0, center=true);
}

radius = 0.45;
angles = [0, 180];
width = 0.1;
fn = 24;

module sector(radius, angles, fn = 100) {
    r = radius / cos(180 / fn);
    step = -360 / fn;

    points = concat([[0, 0]],
        [for(a = [angles[0] : step : angles[1] - 360]) 
            [r * cos(a), r * sin(a)]
        ],
        [[r * cos(angles[1]), r * sin(angles[1])]]
    );

    difference() {
        circle(radius, $fn = fn);
        polygon(points);
    }
}

module arc(radius, angles, width = 1, fn = 100) {
    difference() {
        sector(radius + width, angles, fn);
        sector(radius, angles, fn);
    }
} 

translate([-0.5,0,0]) {
linear_extrude(0.2) {arc(radius, angles, width);}
}

rotate([180,0,0]){
translate([0.5,0,-0.2]) {
linear_extrude(0.2) {arc(0.45, angles, width);}
}}
