## File descriptions

headphones_rest_3:
Designed to sit on top of something like a Corsair gaming headset stand to prevent a rectangular indentation from developing on the inner memory foam pad of the headset.

## License
Creative Commons (4.0 International License) Attribution
